# AnyHedge Stats

[Click here](plots/plots.md) to go directly to rendered stats page.

AnyHedge allows users to take leveraged long or short positions against main-chain BCH.
It is permissionless, non-custodial, and on-chain.
The contracts are fully funded on creation and have no counterparty risk.
The risks are early liquidation, slippage, and oracle risks.

This repository contains scripts that allow anyone to scrape on-chain data and generate statistics based on ALL contract spends that match the AnyHedge contract template, and reproduce the statistics presented [here](plots/plots.md).

**Contract version v0.11**

- Contract code: https://gitlab.com/GeneralProtocols/anyhedge/contracts/-/blob/development/contracts/v0.11/bytecode.asm
- Whitepaper: https://gitlab.com/GeneralProtocols/anyhedge/whitepaper/-/blob/master/anyhedge.md#17-supplement-a-detailed-example-execution-of-anyhedge-on-bitcoin-cash
- Supplement: A detailed example execution of AnyHedge on Bitcoin Cash: https://gitlab.com/GeneralProtocols/anyhedge/whitepaper/-/blob/master/anyhedge-supplement.md

**Oracles**

- https://oracles.cash/

## Requirements

Linux with: `coreutils`, `jq`, `curl`, `gcc`, `gnuplot`, `sqlite3`

## Installation

Simply clone the repo and run the `make.sh` script.
It will:

- Compile the 2 small utils (`bin2scriptnum` and `parse_pushes`) required to parse input scripts
- Create the SQLite database according to `anyhedge_stats.sql` schema
- Import oracle information and already scraped contract data `.csv`s into the database
- Generate aggregate stats `.csv`s
- Generate plots using `gnuplot`

```
git clone https://gitlab.com/0353F40E/anyhedge-stats.git
cd anyhedge-stats
./make.sh
```

## Usage

To stay in sync, simply run the `daily_job.sh` once per day.
It will:

- Query [try.chaingraph.cash](https://try.chaingraph.cash) for block height at `current-10`
- Pull missing blocks data from chaingraph and populate the database with fresh information
- Generate aggregate stats `.csv`s
- Generate plots using `gnuplot`

```
./daily_job.sh
```

Note: plot script will generate charts until `last contract's date - 1`, so to have fresh plots run it later than 4AM UTC so it's sure to get at least 1 contract from the current day.

## License

To the extent possible under law, this work has waived all copyright and related or neighboring rights to this work under [CC0](https://creativecommons.org/publicdomain/zero/1.0/).
