#include <stdio.h>
#include <stdint.h>

int main(int argc, char *argv[])
{
    uint8_t len;
    if (argc > 1 && sscanf(argv[1], "%u", &len) == 1 && len > 0 && len <= 8) {
        int c;
        uint64_t number = 0;
        for (int i = 0; i < len*8; i+=8) {
            c = getchar();
            if (c == EOF)
                return 1;
            number += (uint64_t) c << i;
        }
        uint64_t sign = number & (1ull << (len*8-1));
        if (sign)
            printf("%ld", (int64_t) 0 - (number & ~sign));
        else
            printf("%lu", number);
        putchar('\n');
        return 0;
    }
    else {
        return 1;
    }
}
