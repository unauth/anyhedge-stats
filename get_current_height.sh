#!/bin/bash

function generate_query()
{
  cat <<EOF
query {
  block(
    limit: 1
    offset: 10
    order_by: { height: desc }
    where: {
      accepted_by: { node: { name: { _eq: "bchn-mainnet" } } }
      height: { _gte: 743774 }
    }
  ) {
    height
  }
}
EOF
}

query=$(generate_query | jq -Rsa)
query='{"query":'"$query"'}'

result=$(curl -s 'https://demo.chaingraph.cash/v1/graphql' \
-H 'Accept-Encoding: gzip, deflate, br' \
-H 'Content-Type: application/json' \
-H 'Accept: application/json' \
-H 'Connection: keep-alive' \
-H 'DNT: 1' \
-H 'Origin: https://try.chaingraph.cash' \
--data-binary "$query" \
--compressed)

echo "$result" | jq -r '.data.block[0].height'
