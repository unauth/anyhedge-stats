lastdb=$(sqlite3 anyhedge_stats.sqlite 'select max(close_block_height)+1 from anyhedge_closed_contracts')
if [ -z $lastdb ]
then
    lastdb=743774
fi
./pull_data.sh $lastdb $(./get_current_height.sh) 10 anyhedge_stats.sqlite anyhedge_closed_contracts
./export_closed_contracts.sh
./export_oracle.sh 
./export_stats_daily.sh 
./plot_stats_daily.sh
