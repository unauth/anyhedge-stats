### Closed Contracts Volume

#### Daily Volume Total

<img src=volume_closed.png></img>

#### Daily Volume By Pair

<img src=volume_closed_by_pair.png></img>

#### Cumulative Volume Total

<img src=volume_closed_cumulative.png></img>

#### Cumulative Volume By Pair

<img src=volume_closed_cumulative_by_pair.png></img>

### Opened And Closed Cumulative Volumes

Note, due to AnyHedge contracts being secret until settled, the data lags by 30d.
The difference between the `volume_opened_cumulative` and `volume_closed_cumulative` will give us the Total Value Locked (plotted below).

<img src=volume_open_closed_cumulative.png></img>

### Total Value Locked (TVL)

Note, due to AnyHedge contracts being secret until settled, the data lags by 30d.

#### TVL Total

<img src=tvl.png></img>

#### TVL By Pair

<img src=tvl_by_pair.png></img>