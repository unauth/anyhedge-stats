#!/bin/bash

height_start=$1
height_end=$2
query_blocks=$3
database_file_name=$4
table_name=$5

function generate_query()
{
  cat <<EOF
# Matches all spends from AnyHedge v0.11 contract template
# https://gitlab.com/GeneralProtocols/anyhedge/contracts/-/blob/development/contracts/v0.11/bytecode.asm
query {
  input(
    #limit: 10
    #offset: 0
    where: {
      transaction: {
        block_inclusions: {
          block: {
            height: { _gte: 780000, _lt: 780050 }
            accepted_by: { node: { name: { _eq: "bchn-mainnet" } } }
          }
        }
      }
      _or: [
        { unlocking_bytecode_pattern: { _eq: "40104010514d" } }
        { unlocking_bytecode_pattern: { _eq: "4141004d" } }
      ]
      redeem_bytecode_pattern: {
        _regex: "04040[2-4]0[2-4]0[2-8]0[2-8]21(17|19|23)(17|19|23)(00|51)21215c79009c637b695c7a7cad5b7a7cad6d6d6d6d6d51675c7a519dc3519d5f7a5f795779bb5d7a5d79577abb5c79587f77547f75817600a0695c79587f77547f75818c9d5c7a547f75815b799f695b795c7f77817600a0695979a35879a45c7a547f7581765c7aa2695b7aa2785a7a8b5b7aa5919b690276587a537a96a47c577a527994a4c4529d00cc7b9d00cd557a8851cc9d51cd547a8777777768"
      }
    }
  ) {
    transaction {
      block_inclusions {
        block {
          height
          timestamp
        }
      }
      inputs {
        outpoint_transaction_hash
        outpoint_index
        outpoint {
          value_satoshis
          locking_bytecode
          locking_bytecode_pattern
          transaction {
            block_inclusions {
              block {
                height
                timestamp
              }
            }
          }
        }
        unlocking_bytecode
        unlocking_bytecode_pattern
        redeem_bytecode_pattern
      }
    }
  }
}
EOF
}

# args: query, limit, offset, height_gte, height_lt
function update_query_parameters () {
    local q="$1"
    q=$(echo "$q" | sed -r -e 's/limit:\ [0-9]+/limit:\ '$2'/')
    q=$(echo "$q" | sed -r -e 's/offset:\ [0-9]+/offset:\ '$3'/')
    q=$(echo "$q" | sed -r -e 's/_gte:\ [0-9]+/_gte:\ '$4'/')
    q=$(echo "$q" | sed -r -e 's/_lt:\ [0-9]+/_lt:\ '$5'/')
    echo "$q"
}

# args: database_file_name, table_name
function csv_to_sqlite() {
  sqlite3 -csv "$1" ".import '|cat -' $2"
}

# args: unlocking_bytecode
# output: csv of AnyHedge input script data (redeem script omitted)
function get_input_script_field() {
    local pushes=$(echo "$1" | xxd -r -p | ./parse_pushes -d)
    readarray <<<"$pushes" pushes
    if [ ${#pushes[@]} -eq 6 ]
    then
        pushes=( "${pushes[@]:4:1}" "${pushes[@]:0:4}" "" "" )
        echo -n "${pushes[@]:0:7}" | tr ' ' ',' | tr -d '\n'
    elif [ ${#pushes[@]} -eq 4 ]
    then
        pushes=( "${pushes[@]:2:1}" "" "" "" "" "${pushes[@]:0:2}" )
        echo -n "${pushes[@]:0:7}" | tr ' ' ',' | tr -d '\n'
    fi
}
export -f get_input_script_field

# args: unlocking_bytecode
# output: csv of AnyHedge redeem script parameters
function get_redeem_script_field() {
    local pushes=$(echo "$1" | xxd -r -p | ./parse_pushes | tail -n 1 | cut -b 3- | xxd -r -p | ./parse_pushes -d)
    readarray <<<"$pushes" pushes
    echo -n "${pushes[@]:0:12}" | tr ' ' ',' | tr -d '\n'
}
export -f get_redeem_script_field

# args: unlocking_bytecode
# output: csv of the 4 oracle price message fields
function parse_oracle_message() {
    local ofields=(\
        $(echo "$1" | cut -c 3-10 | xxd -r -p | ./bin2scriptnum 4)\
        $(echo "$1" | cut -c 11-18 | xxd -r -p | ./bin2scriptnum 4)\
        $(echo "$1" | cut -c 19-26 | xxd -r -p | ./bin2scriptnum 4)\
        $(echo "$1" | cut -c 27-34 | xxd -r -p | ./bin2scriptnum 4)\
    )
    echo -n "${ofields[@]}" | tr ' ' ',' | tr -d '\n'
}
export -f parse_oracle_message

query=$(generate_query | jq -Rsa)
query='{"query":'"$query"'}'

height=$height_start
while [ $height -le $((height_end-query_blocks+1)) ]
do
    limit=0
    offset=0
    height_gte=$height
    height_lt=$((height+query_blocks))
    echo "query_blocks_range ["$height_gte", "$height_lt">" >> /dev/stderr
    query=$(update_query_parameters "$query" $limit $offset $height_gte $height_lt)
    result=$(curl -s 'https://demo.chaingraph.cash/v1/graphql' \
    -H 'Accept-Encoding: gzip, deflate, br' \
    -H 'Content-Type: application/json' \
    -H 'Accept: application/json' \
    -H 'Connection: keep-alive' \
    -H 'DNT: 1' \
    -H 'Origin: https://try.chaingraph.cash' \
    --data-binary "$query" \
    --compressed)
    # echo "$result" >> /dev/stderr
    errors=$(echo "$result" | jq -r '.errors | length')
    echo $errors' errors' >> /dev/stderr
    if [ "$errors" -ne "0" ]
    then
        exit 1
    fi
    rows=$(echo "$result" | jq -r '.data.input | length')
    echo $rows' rows' >> /dev/stderr
    echo "$result" | jq -r '.data.input[] | map([.inputs[].outpoint_transaction_hash, .inputs[].outpoint_index, .inputs[].outpoint.value_satoshis, .inputs[].outpoint.locking_bytecode, .inputs[].outpoint.locking_bytecode_pattern, .inputs[].outpoint.transaction.block_inclusions[].block.height, .inputs[].outpoint.transaction.block_inclusions[].block.timestamp, .block_inclusions[].block.height, .block_inclusions[].block.timestamp, .inputs[].unlocking_bytecode, .inputs[].unlocking_bytecode_pattern, .inputs[].redeem_bytecode_pattern] | join(",")) | join("\n")' \
    | awk  -F "," '{OFS=","; "get_input_script_field " $10 | getline rp1; "get_redeem_script_field " $10 | getline rp2; print $1,$2,$3,$6,$7,$8,$9,rp1,rp2}' \
    | awk -F "," '{OFS=","; "parse_oracle_message " $(NF-16) | getline o1; "parse_oracle_message " $(NF-14) | getline o2; print $0,(($(NF-18)==1)?o1:",,,"),(($(NF-18)==1)?o2:",,,")}' \
    | csv_to_sqlite "$database_file_name" "$table_name" 2>&1 | grep -v 'UNIQUE constraint failed' 1>&2
    height=$((height+query_blocks))
    sleep 1
done
